import 'package:flutter/material.dart';
import 'package:test_task/pages/enter_email_page/enter_email_page.dart';

void main() {
  runApp(const App());
}

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(home: EnterEmailPage());
  }
}
