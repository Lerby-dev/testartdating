abstract class AppString{
  static const enterEmailText = "Enter your Email Address";
  static const hintEmailText = "Email";
  static const continueButtonText = "Continue";
  static const aboutMeTitleText = "About Me:";
  static const galleryName = "Gallery Name";
  static const selectCountryText = "Select Country";
  static const selectCityText = "Select City";
  static const galleryTypeText = "Gallery type";
  static const artClassification = "Art Classification";
  static const profileDescription = "Profile description";

}