import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:test_task/pages/about_me_page/about_me_bloc.dart';
import 'package:test_task/utils/app_strings.dart';

import '../../res.dart';
import '../../widgets/widgets.dart';

class AboutMePage extends StatelessWidget {
  const AboutMePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => AboutMeBloc(),
      child: const AboutMeView(),
    );
  }
}

class AboutMeView extends StatefulWidget {
  const AboutMeView({Key? key}) : super(key: key);

  @override
  State<AboutMeView> createState() => _AboutMeViewState();
}

class _AboutMeViewState extends State<AboutMeView> {
  String? dropdownValue ;
  String? dropdownValueCity;
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return BlocBuilder<AboutMeBloc, String>(builder: (context, state) {
      return Scaffold(
        body: Container(
          width: width,
          height: height,
          decoration: const BoxDecoration(
        gradient: LinearGradient(
        begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xFF326A7D),
            Color(0xFF011E28),
          ],
        )),
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
              SizedBox(height: 65),
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0, ),
                child: SizedBox(
                  width: width,
                  child: SvgPicture.asset(
                    Res.logo,
                    semanticsLabel: 'Logo',
                    height: 30,
                    width: 155,
                  ),
                ),
              ),
                SizedBox(height: 40,),
                SizedBox(
                  width: 320,
                    child: Text(AppString.aboutMeTitleText, style: GoogleFonts.roboto(fontWeight: FontWeight.bold, fontSize: 26, color: Colors.white),)),
                Padding(
                  padding: EdgeInsets.only(top: 20.0,),
                  child: SizedBox(
                    width: 320,
                    height: 46,
                    child: TextField(
                      textAlign: TextAlign.start,
                      obscureText: false,
                      style: TextStyle(color: Colors.white, fontSize: 12),
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.only(left: 20.0),
                        filled: true,
                        hintText: AppString.galleryName,
                        hintStyle: TextStyle(color: Colors.white, fontSize: 12),
                        fillColor: Color(0xFF316C80),
                        border: OutlineInputBorder(borderRadius: BorderRadius.circular(12), borderSide: BorderSide(width: 1, color: Colors.transparent)),
                        enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(12), borderSide: BorderSide(width: 1, color: Colors.transparent)),
                        disabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(12), borderSide: BorderSide(width: 1, color: Colors.transparent)),
                        focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(12), borderSide: BorderSide(width: 1, color: Colors.transparent)),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20.0,),
                  child: SizedBox(
                    width: 320,
                    height: 46,
                    child: DropdownButtonFormField(
                      iconSize: 24,
                      icon: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,

                          children: [

                        VerticalDivider(
                          color: Color(0xFF63ACC2),
                          thickness: 2,
                        ),
                        Center(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: RotatedBox(
                              quarterTurns: 3,
                              child: Icon(Icons.arrow_back_ios_new, color: Color(0xFF63ACC2), size: 20,
                              ),
                            ),
                          ),
                        )]),
                      value: dropdownValue,
                      dropdownColor: Color(0xFF316C80),
                      style: TextStyle(color: Colors.white, fontSize: 12),
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.only(left: 20.0),
                        filled: true,
                        hintText: AppString.selectCountryText,
                        hintStyle: TextStyle(color: Colors.white, fontSize: 12),
                        fillColor: Color(0xFF316C80),
                        border: OutlineInputBorder(borderRadius: BorderRadius.circular(12), borderSide: BorderSide(width: 1, color: Colors.transparent)),
                        enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(12), borderSide: BorderSide(width: 1, color: Colors.transparent)),
                        disabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(12), borderSide: BorderSide(width: 1, color: Colors.transparent)),
                        focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(12), borderSide: BorderSide(width: 1, color: Colors.transparent)),

                      ), items: <String>['Ukraine', 'USA', 'Canada', 'Spain'].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(
                          value,
                          style: TextStyle(fontSize: 12),
                        ),
                      );
                    }).toList(), onChanged: (String? newValue) {
                      setState(() {
                        dropdownValue = newValue!;
                      });
                    },
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20.0,),
                  child: SizedBox(
                    width: 320,
                    height: 46,
                    child: DropdownButtonFormField(
                      isExpanded: true,

                      icon: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,

                          children: [

                        VerticalDivider(
                          color: Color(0xFF63ACC2),
                          thickness: 2,
                        ),
                        Center(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: RotatedBox(
                              quarterTurns: 3,
                              child: Icon(Icons.arrow_back_ios_new, color: Color(0xFF63ACC2), size: 20,
                              ),
                            ),
                          ),
                        )]),
                      hint: Text(AppString.selectCityText , style: TextStyle(color: Colors.white, fontSize: 12),),
                      value: dropdownValueCity,
                      dropdownColor: Color(0xFF316C80),
                      style: TextStyle(color: Colors.white, fontSize: 12),
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.only(left: 20.0),
                        filled: true,
                        hintText: AppString.selectCityText,
                        hintStyle: TextStyle(color: Colors.white, fontSize: 12),
                        fillColor: Color(0xFF316C80),
                        border: OutlineInputBorder(borderRadius: BorderRadius.circular(12), borderSide: BorderSide(width: 1, color: Colors.transparent)),
                        enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(12), borderSide: BorderSide(width: 1, color: Colors.transparent)),
                        disabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(12), borderSide: BorderSide(width: 1, color: Colors.transparent)),
                        focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(12), borderSide: BorderSide(width: 1, color: Colors.transparent)),

                      ), items: <String>['Kharkov', 'Kiev', 'Lviv'].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(
                          value,
                          style: TextStyle(fontSize: 12),
                        ),
                      );
                    }).toList(), onChanged: (String? newValue) {
                      setState(() {
                        dropdownValueCity = newValue!;
                      });
                    },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top:20.0),
                  child: SizedBox(
                      width: 320,
                      child: Text(AppString.galleryTypeText, style: GoogleFonts.roboto(fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white),)),
                ),
                Padding(
                  padding: const EdgeInsets.only(top:20.0),
                  child: SizedBox(
                    width: 320,
                      height:80,
                      child: CustomTagsFormField(listInitialTag: [
                        'Gallery-salon', 'Virtual'
                      ],)),
                ),
                Padding(
                  padding: const EdgeInsets.only(top:20.0),
                  child: SizedBox(
                      width: 320,
                      child: Text(AppString.artClassification, style: GoogleFonts.roboto(fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white),)),
                ),
                Padding(
                  padding: const EdgeInsets.only(top:20.0),
                  child: SizedBox(
                    width: 320,
                      height:80,
                      child: CustomTagsFormField(listInitialTag: [
                        'Gallery-salon', 'Virtual'
                      ],)),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20.0,),
                  child: SizedBox(
                    width: 320,
                    height: 120,
                    child: TextField(
                      maxLines: 10,

                      textAlign: TextAlign.start,
                      obscureText: false,
                      style: TextStyle(color: Colors.white, fontSize: 12),
                      decoration: InputDecoration(

                        contentPadding: EdgeInsets.only(left: 20.0, top: 12),
                        filled: true,
                        hintText: AppString.profileDescription,
                        hintStyle: TextStyle(color: Colors.white, fontSize: 12),
                        fillColor: Color(0xFF316C80),
                        border: OutlineInputBorder(borderRadius: BorderRadius.circular(12), borderSide: BorderSide(width: 1, color: Colors.transparent)),
                        enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(12), borderSide: BorderSide(width: 1, color: Colors.transparent)),
                        disabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(12), borderSide: BorderSide(width: 1, color: Colors.transparent)),
                        focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(12), borderSide: BorderSide(width: 1, color: Colors.transparent)),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0, left: 35, right: 35),
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          elevation: 0,
                          backgroundColor: Color(0xFFFF7A00),
                          shape: RoundedRectangleBorder(
                              side: BorderSide.none,
                              borderRadius: BorderRadius.all(
                                Radius.circular(12),
                              ))),
                      onPressed: () => {},
                      child: SizedBox(
                          width: 285,
                          height: 46,
                          child: Center(
                              child: Text(
                                AppString.continueButtonText,
                                style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                              )))),
                )
            ],),
          ),
        ),
      );
    });
  }
}
