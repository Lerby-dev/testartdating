import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:route_transitions/route_transitions.dart';
import 'package:test_task/utils/app_strings.dart';

import '../../res.dart';
import '../about_me_page/ebout_me_page.dart';
import 'enter_email_bloc.dart';

class EnterEmailPage extends StatelessWidget {
  const EnterEmailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(create: (_) => EnterEmailBloc(), child: const EnterEmailView());
  }
}

class EnterEmailView extends StatefulWidget {
  const EnterEmailView({Key? key}) : super(key: key);

  @override
  State<EnterEmailView> createState() => _EnterEmailViewState();
}

class _EnterEmailViewState extends State<EnterEmailView> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return BlocBuilder<EnterEmailBloc, String>(builder: (context, email) {
      return Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.startTop,
        floatingActionButton:  ElevatedButton(
            style: ElevatedButton.styleFrom(shadowColor: Colors.transparent, backgroundColor: Color(0xFF31768D), shape: CircleBorder()),
            onPressed: () => {},
            child: SizedBox(
              width: 45,
              height: 45,
              child: Icon(
                Icons.arrow_back,
                color: Colors.white,
                size: 22,
              ),
            )),
        body: Container(
          width: width,
          height: height,
          decoration: const BoxDecoration(
              gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color(0xFF326A7D),
              Color(0xFF011E28),
            ],
          )),
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 65),
                Padding(
                  padding: const EdgeInsets.only(bottom: 8.0,),
                  child: SvgPicture.asset(
                    Res.logo,
                    semanticsLabel: 'Logo',
                    height: 30,
                    width: 155,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 24.0),
                  child: SvgPicture.asset(Res.email_image, semanticsLabel: 'Email Image'),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Text(
                    AppString.enterEmailText,
                    style: GoogleFonts.roboto(fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 40.0),
                  child: SizedBox(
                    width: 320,
                    height: 46,
                    child: TextField(
                      textAlign: TextAlign.start,
                      obscureText: false,
                      style: TextStyle(color: Colors.white, fontSize: 12),
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.only(left: 20.0),
                        filled: true,
                        hintText: AppString.hintEmailText,
                        hintStyle: TextStyle(color: Colors.white, fontSize: 12),
                        fillColor: Color(0xFF316C80),
                        border: OutlineInputBorder(borderRadius: BorderRadius.circular(12), borderSide: BorderSide(width: 1, color: Colors.transparent)),
                        enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(12), borderSide: BorderSide(width: 1, color: Colors.transparent)),
                        disabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(12), borderSide: BorderSide(width: 1, color: Colors.transparent)),
                        focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(12), borderSide: BorderSide(width: 1, color: Colors.transparent)),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0, left: 35, right: 35),
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          elevation: 0,
                          backgroundColor: Color(0xFFFF7A00),
                          shape: RoundedRectangleBorder(
                              side: BorderSide.none,
                              borderRadius: BorderRadius.all(
                                Radius.circular(12),
                              ))),
                      onPressed: () => {fadeWidget(newPage: AboutMePage(), context: context)},
                      child: SizedBox(
                          width: 285,
                          height: 46,
                          child: Center(
                              child: Text(
                            AppString.continueButtonText,
                            style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                          )))),
                )
              ],
            ),
          ),
        ),
      );
    });
  }
}
