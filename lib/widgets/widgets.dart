import 'package:flutter/material.dart';
import 'package:textfield_tags/textfield_tags.dart';
class CustomTagsFormField extends StatefulWidget {
  const CustomTagsFormField({super.key, required this.listInitialTag});
  final List<String> listInitialTag;
  @override
  State<CustomTagsFormField> createState() => _CustomTagsFormFieldState();
}

class _CustomTagsFormFieldState extends State<CustomTagsFormField> {
  late double _distanceToField;
  late TextfieldTagsController _controller;


  _CustomTagsFormFieldState();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _distanceToField = MediaQuery.of(context).size.width;
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  void initState() {
    super.initState();
    _controller = TextfieldTagsController();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
            children: [
              TextFieldTags(
                textfieldTagsController: _controller,
                initialTags: widget.listInitialTag,
                textSeparators: const [' ', ','],
                letterCase: LetterCase.normal,
                validator: (String tag) {
                  if (_controller.getTags!.contains(tag)) {
                    return 'you already entered that';
                  }
                  return null;
                },
                inputfieldBuilder:
                    (context, tec, fn, error, onChanged, onSubmitted) {
                  return ((context, sc, tags, onTagDelete) {
                    return TextField(
                      controller: tec,
                      focusNode: fn,
                      style: TextStyle(color: Colors.white, fontSize: 12),
                      decoration: InputDecoration(

                        isDense: true,

                        border: const OutlineInputBorder(
                          borderSide: BorderSide(
                            color:  Color(0xFF9BD6E8),
                            width: 1.0,
                          ),
                            borderRadius: BorderRadius.all(Radius.circular(12))
                        ),
                        focusedBorder: const OutlineInputBorder(
                          borderSide: BorderSide(
                            color:  Color(0xFF9BD6E8),
                            width: 1.0,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(12)),

                        ),
                        enabledBorder: const OutlineInputBorder(
                            borderSide: BorderSide(
                              color:  Color(0xFF9BD6E8),
                              width: 1.0,
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(12))
                        ),
                        helperText: 'Select several items from the list',
                        helperStyle: const TextStyle(
                          color: Color(0xFF9BD6E8),
                        ),
                        hintText: _controller.hasTags ? '' : "Enter tag...",
                        hintStyle: TextStyle(color: Colors.white, fontSize: 12),
                        errorText: error,
                        prefixIconConstraints:
                        BoxConstraints(maxWidth: _distanceToField * 0.74),
                        prefixIcon: tags.isNotEmpty
                            ? SingleChildScrollView(
                          controller: sc,
                          scrollDirection: Axis.horizontal,
                          child: Padding(
                            padding: const EdgeInsets.all(2.0),
                            child: Row(
                                children: tags.map((String tag) {
                                  return Container(
                                    decoration: const BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(8.0),
                                      ),
                                      color: Color(0xFF255C6F),
                                    ),
                                    margin: const EdgeInsets.symmetric(
                                        horizontal: 3.0, vertical: 6.0),

                                    child: Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(top: 12.0, bottom: 12, left: 12, right: 0),
                                          child: InkWell(
                                            child: Text(
                                              tag,
                                              style: const TextStyle(
                                                  color: Colors.white),
                                            ),
                                            onTap: () {
                                              print("$tag selected");
                                            },
                                          ),
                                        ),
                                        const SizedBox(width: 14.0),
                                        Padding(
                                          padding: const EdgeInsets.only(right: 9.0),
                                          child: InkWell(
                                            child: const Icon(
                                              Icons.clear,
                                              size: 18.0,
                                              color: Color(0xFF9BD6E8),
                                            ),
                                            onTap: () {
                                              onTagDelete(tag);
                                            },
                                          ),
                                        )
                                      ],
                                    ),
                                  );
                                }).toList()),
                          ),
                        )
                            : null,
                      ),
                      onChanged: onChanged,
                      onSubmitted: onSubmitted,
                    );
                  });
                },
              ),
            ],

      ),
    );
  }
}
